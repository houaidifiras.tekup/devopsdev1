<?php
namespace App\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Entity\Location;
use App\Entity\Voiture;
use App\Entity\Client;

class LocationIntegrationTest extends KernelTestCase
{
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    public function testLocationCRUD()
    {
        // Création d'une voiture pour associer à la location
        $voiture = new Voiture();
        $voiture
            ->setSerie('ExempleSerie')
            ->setModele('Example Model')
            ->setPrixJour(100)
            ->setDateMiseEn(new \DateTime());

        // Création d'un client pour associer à la location
        $client = new Client();
        $client
            ->setNom('John Doe')
            ->setPrenom('Doe')
            ->setCin('123456')
            ->setAdresse('123 Rue Example')
            ->addLocation(new Location());

        // Création d'une nouvelle location
        $location = new Location();
        $location
            ->setDateDebut(new \DateTime()) // Définir la date de début
            ->setDateRetour(new \DateTime('+7 days'))
            ->setPrix(100)
            ->setVoiture($voiture)
            ->setClient($client);

        // Persister la nouvelle location
        $this->entityManager->persist($voiture);
        $this->entityManager->persist($client);
        $this->entityManager->persist($location);
        $this->entityManager->flush();

        // Récupérer l'ID de la nouvelle location
        $locationId = $location->getId();

        // Récupérer la location depuis la base de données
        $persistedLocation = $this->entityManager->getRepository(Location::class)->find($locationId);

        // Vérification que l'entité récupérée correspond à celle que nous avons créée
        $this->assertEquals($location->getDateDebut(), $persistedLocation->getDateDebut());
        $this->assertEquals($location->getDateRetour(), $persistedLocation->getDateRetour());
        $this->assertEquals($location->getPrix(), $persistedLocation->getPrix());

        // Assertions pour d'autres propriétés de l'entité Location
        $this->assertInstanceOf(Voiture::class, $persistedLocation->getVoiture());
        $this->assertInstanceOf(Client::class, $persistedLocation->getClient());
        // ... ajouter d'autres assertions pour vérifier d'autres propriétés de l'entité

        // Supprimer la location de la base de données
        $this->entityManager->remove($persistedLocation);
        $this->entityManager->flush();

        // Vérifier que la location a été correctement supprimée en essayant de la récupérer à nouveau
        $removedLocation = $this->entityManager->getRepository(Location::class)->find($locationId);
        $this->assertNull($removedLocation);
    }
}
