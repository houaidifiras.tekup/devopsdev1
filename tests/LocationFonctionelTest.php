<?php

namespace App\Tests;

use App\Entity\Location;
use App\Entity\Client;
use App\Entity\Voiture;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LocationFonctionelTest extends WebTestCase
{
    public function testCreationLocation(): void
    {
        self::bootKernel();
        $entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();

        // Création d'une nouvelle location
        $location = new Location();
        $location
            ->setDateDebut(new \DateTime('2023-12-01'))
            ->setDateRetour(new \DateTime('2023-12-10'))
            ->setPrix(500); // Définissez le prix selon vos besoins

        // Récupération d'une instance de Voiture et de Client (à adapter en fonction de vos données)
        $voiture = $entityManager->getRepository(Voiture::class)->findOneBy([]);
        $client = $entityManager->getRepository(Client::class)->findOneBy([]);

        // Assurez-vous que Voiture et Client existent avant de les associer à la location
        if ($voiture && $client) {
            $location->setVoiture($voiture)
                ->setClient($client);

            $entityManager->persist($location);
            $entityManager->flush();

            $this->assertNotNull($location->getId());
            // Ajoutez d'autres assertions selon les propriétés que vous voulez tester
        } else {
            $this->markTestSkipped('Voiture ou Client non trouvé pour associer à la Location.');
        }
    }

    // Ajoutez d'autres méthodes de test selon les fonctionnalités que vous souhaitez tester
}
