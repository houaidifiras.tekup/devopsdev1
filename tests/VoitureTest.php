<?php

namespace App\Tests\Entity;

use App\Entity\Voiture;
use App\Entity\Location;
use PHPUnit\Framework\TestCase;

require_once 'bootstrap.php'; // Assurez-vous d'inclure votre fichier bootstrap si nécessaire

class VoitureTest extends TestCase
{
    public function testCanBeCreated(): void
    {
        $voiture = new Voiture();
        // Testez les assertions ici pour vérifier les comportements attendus de l'entité Voiture
        $this->assertInstanceOf(Voiture::class, $voiture);
        // Ajoutez d'autres assertions selon vos besoins spécifiques
    }

    public function testAddLocation(): void
    {
        $voiture = new Voiture();
        $location = new Location();

        // Ajoutez la voiture à la location
        $voiture->addLocation($location);

        // Vérifiez que la voiture a bien été ajoutée à la location
        $this->assertSame($voiture, $location->getVoiture());
        // Vérifiez également que la location a été ajoutée à la voiture
        $this->assertTrue($voiture->getLocation()->contains($location));
    }

    public function testRemoveLocation(): void
    {
        $voiture = new Voiture();
        $location = new Location();

        // Ajoutez la voiture à la location
        $voiture->addLocation($location);

        // Retirez la voiture de la location
        $voiture->removeLocation($location);

        // Vérifiez que la voiture a bien été retirée de la location
        $this->assertNull($location->getVoiture());
        // Vérifiez également que la location a été retirée de la voiture
        $this->assertFalse($voiture->getLocation()->contains($location));
    }

    // Ajoutez d'autres méthodes de test pour tester différentes fonctionnalités de votre entité Voiture
}
