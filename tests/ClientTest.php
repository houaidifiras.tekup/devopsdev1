<?php

namespace App\Tests\Entity;

use App\Entity\Client;
use App\Entity\Location;
use PHPUnit\Framework\TestCase;

require_once 'bootstrap.php'; // Assurez-vous d'inclure votre fichier bootstrap si nécessaire

class ClientTest extends TestCase
{
    public function testCanBeCreated(): void
    {
        $client = new Client();
        // Testez les assertions ici pour vérifier les comportements attendus de l'entité Client
        $this->assertInstanceOf(Client::class, $client);
        // Ajoutez d'autres assertions selon vos besoins spécifiques
    }

    public function testSetGetCin(): void
    {
        $client = new Client();
        $cin = '12345678'; // Exemple de numéro de CIN
        $client->setCin($cin);

        // Vérifiez si le CIN est correctement défini
        $this->assertSame($cin, $client->getCin());
    }

    public function testSetGetNom(): void
    {
        $client = new Client();
        $nom = 'Doe'; // Exemple de nom
        $client->setNom($nom);

        // Vérifiez si le nom est correctement défini
        $this->assertSame($nom, $client->getNom());
    }

    public function testSetGetPrenom(): void
    {
        $client = new Client();
        $prenom = 'John'; // Exemple de prénom
        $client->setPrenom($prenom);

        // Vérifiez si le prénom est correctement défini
        $this->assertSame($prenom, $client->getPrenom());
    }

    public function testSetGetAdresse(): void
    {
        $client = new Client();
        $adresse = '123, rue Example'; // Exemple d'adresse
        $client->setAdresse($adresse);

        // Vérifiez si l'adresse est correctement définie
        $this->assertSame($adresse, $client->getAdresse());
    }

    // Ajoutez d'autres méthodes de test pour tester différentes fonctionnalités de votre entité Client
}
