<?php

namespace App\Tests;

use App\Entity\Client;
use App\Entity\Location;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClientFonctionnelTest extends WebTestCase
{
    public function testCreationClient(): void
    {
        self::bootKernel();
        $entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();

        $client = new Client();
        $client
            ->setCin('12345')
            ->setNom('Doe')
            ->setPrenom('John')
            ->setAdresse('123 Main Street');

        $entityManager->persist($client);
        $entityManager->flush();

        $this->assertNotNull($client->getId());
        // Ajoutez d'autres assertions selon les propriétés que vous voulez tester
    }

    public function testAddLocationToClient(): void
    {
        self::bootKernel();
        $entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();

        $client = new Client();
        $client
            ->setCin('54321')
            ->setNom('Smith')
            ->setPrenom('Jane')
            ->setAdresse('456 Elm Street');

        $location = new Location(); // Assurez-vous d'avoir la classe Location pour pouvoir créer une instance

        // Ajoutez une location à un client
        $client->addLocation($location);

        $entityManager->persist($client);
        $entityManager->flush();

        $this->assertCount(1, $client->getLocations());
        // Ajoutez d'autres assertions pour tester les relations et les propriétés
    }
}
