<?php

namespace App\Tests\Entity;

use App\Entity\Location;
use App\Entity\Voiture;
use App\Entity\Client;
use PHPUnit\Framework\TestCase;

require_once 'bootstrap.php'; // Assurez-vous d'inclure votre fichier bootstrap si nécessaire

class LocationTest extends TestCase
{
    public function testCanBeCreated(): void
    {
        $location = new Location();
        // Testez les assertions ici pour vérifier les comportements attendus de l'entité Location
        $this->assertInstanceOf(Location::class, $location);
        // Ajoutez d'autres assertions selon vos besoins spécifiques
    }

    public function testSetGetDateDebut(): void
    {
        $location = new Location();
        $dateDebut = new \DateTime('2023-12-01');
        $location->setDateDebut($dateDebut);

        // Vérifiez si la date de début est correctement définie
        $this->assertSame($dateDebut, $location->getDateDebut());
    }

    public function testSetGetDateRetour(): void
    {
        $location = new Location();
        $dateRetour = new \DateTime('2023-12-10');
        $location->setDateRetour($dateRetour);

        // Vérifiez si la date de retour est correctement définie
        $this->assertSame($dateRetour, $location->getDateRetour());
    }

    public function testSetGetPrix(): void
    {
        $location = new Location();
        $prix = 500;
        $location->setPrix($prix);

        // Vérifiez si le prix est correctement défini
        $this->assertSame($prix, $location->getPrix());
    }

    public function testSetGetVoiture(): void
    {
        $location = new Location();
        $voiture = new Voiture();
        $location->setVoiture($voiture);

        // Vérifiez si la voiture est correctement définie
        $this->assertSame($voiture, $location->getVoiture());
    }

    public function testSetGetClient(): void
    {
        $location = new Location();
        $client = new Client();
        $location->setClient($client);

        // Vérifiez si le client est correctement défini
        $this->assertSame($client, $location->getClient());
    }

    // Ajoutez d'autres méthodes de test pour tester différentes fonctionnalités de votre entité Location
}
