<?php

namespace App\Tests;

use App\Entity\Voiture;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VoitureFonctionnelTest extends WebTestCase
{
    public function testCreationVoiture(): void
    {
        self::bootKernel();
        $entityManager = static::$kernel->getContainer()->get('doctrine')->getManager();

        $voiture = new Voiture();
        $voiture
            ->setSerie('Série XYZ')
            ->setDateMiseEn(new \DateTime())
            ->setModele('Modèle ABC')
            ->setPrixJour(100);

        $entityManager->persist($voiture);
        $entityManager->flush();

        $this->assertNotNull($voiture->getId());
        // Ajoutez d'autres assertions selon les propriétés que vous voulez tester
    }
}
