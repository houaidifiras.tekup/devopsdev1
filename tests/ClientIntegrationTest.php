<?php
namespace App\Tests;

use App\Entity\Client;
use App\Entity\Location;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClientIntegrationTest extends WebTestCase
{
    private $entityManager;

    protected function setUp(): void
    {
        parent::setUp();
        $client = static::createClient();
        $this->entityManager = $client->getContainer()->get('doctrine')->getManager();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->truncateEntities([Client::class, Location::class]);
        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testCreationClient()
    {
        $client = new Client();
        $client
            ->setNom('Doe')
            ->setPrenom('John')
            ->setCin('123456')
            ->setAdresse('123 Rue Example');

        $this->entityManager->persist($client);
        $this->entityManager->flush();

        $this->assertNotNull($client->getId());
    }

    private function truncateEntities(array $entities): void
    {
        $connection = $this->entityManager->getConnection();
        $platform = $connection->getDatabasePlatform();

        $connection->executeStatement('SET FOREIGN_KEY_CHECKS=0;');

        foreach ($entities as $entity) {
            $tableName = $this->entityManager->getClassMetadata($entity)->getTableName();
            $connection->executeStatement($platform->getTruncateTableSQL($tableName, true));
        }

        $connection->executeStatement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
