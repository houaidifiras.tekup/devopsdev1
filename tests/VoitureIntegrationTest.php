<?php
namespace App\Tests;

//use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Entity\Voiture;
use App\Entity\Location;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class VoitureIntegrationTest extends WebTestCase
{
    private $entityManager;

    protected function setUp(): void
    {
        parent::setUp();
        $client = static::createClient();
        $this->entityManager = $client->getContainer()->get('doctrine')->getManager();
        // ... Reste du code inchangé ...
    }
    protected function tearDown(): void
    {
        parent::tearDown();
        // Nettoyer la base de données après chaque test
        $this->truncateEntities([Location::class, Voiture::class]);
        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testCreationVoiture()
    {
        // Créer une instance de Voiture
        $voiture = new Voiture();
        $voiture
            ->setSerie('Série XYZ')
            ->setDateMiseEn(new \DateTime())
            ->setModele('Modèle ABC')
            ->setPrixJour(100);

        // Persister la voiture
        $this->entityManager->persist($voiture);
        $this->entityManager->flush();

        // Vérifier si l'identifiant de la voiture a été généré
        $this->assertNotNull($voiture->getId());
    }

    // Méthode pour vider les entités spécifiées
    private function truncateEntities(array $entities): void
    {
        $connection = $this->entityManager->getConnection();
        $platform = $connection->getDatabasePlatform();

        $connection->executeStatement('SET FOREIGN_KEY_CHECKS=0;');

        // Réinitialiser les tables
        foreach ($entities as $entity) {
            $tableName = $this->entityManager->getClassMetadata($entity)->getTableName();
            $connection->executeStatement($platform->getTruncateTableSQL($tableName, true));
        }

        $connection->executeStatement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
